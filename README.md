# Hypothetical contents of: company/management/organization/README.md

GitLab **company** subgroups (and projects): (examples to show the structure, not necessarily actual subgroups and projects or a complete listing)

**ikuzo** (project) ⬅︎ software project

**proposals**

proposals/**keiretsu** (project) ⬅︎ proposal and related materials, create issue for each change

**specifications**

specifications/**ikuzo** (project) ⬅︎ SRS and related materials, create issue for each change

**management**

management/**organization** (project) ⬅︎ create issues for suggested changes, document in repo

management/**shokunin**

management/shokunin/**hanzo** (project) ⬅︎ stuff related to that person, updated by that person

management/**meetings**

management/meetings/**2020** (project) ⬅︎ meeting notes by date in that year

**partners**

partners/**shinra** ⬅︎ each partner has access to their own subgroup

partners/shinra/**mako** (project) ⬅︎ fork of that product for that partner, partner creates issues

**docs**

docs/**ikuzo** (project) ⬅︎ user manual for that product, should likely be public and anyone can create issues

## Rules

* Only the "boss" can create/grant creation of subgroups and projects (request it by creating an issue in management/organization).
* The "boss" is the owner of every subgroup/project under company and nobody else is (if something needs to be changed then request it by creating an issue in management/organization).
* Don't add hierarchy unless it is necessary to distinguish the context of a project, e.g., ikuzo is a software project while docs/ikuzo is its documentation.
* Labels (e.g., "To Do" and "Doing") should be created on the highest (group) level possible (request new labels by creating an issue in management/organization).
* Generally all content is to be in the main project repos (only use wikis if really needed for non-essential "meta" stuff).
* For contributions to external groups/projects, company as a group should be given guest access (unless they are already public) and links to the groups/projects should be added below (request it by creating an issue in management/organization).
* If you need to track time for an issue, use "/estimate xd xh xm" and "/spend xd xh xm" in comments on that issue.
* Rules can be requested to be added/changed/removed/granted exceptions for/discussed (by creating an issue in management/organization).

## External groups/projects
